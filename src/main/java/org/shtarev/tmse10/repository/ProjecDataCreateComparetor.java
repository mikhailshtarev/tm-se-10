package org.shtarev.tmse10.repository;

import org.shtarev.tmse10.entyty.Project;

import java.util.Comparator;

public class ProjecDataCreateComparetor implements Comparator<Project> {

    public int compare(Project a, Project b) {
        assert a.getDataCreate() != null;
        assert b.getDataCreate() != null;
        int i = a.getDataCreate().compareTo(b.getDataCreate());
        if (i != 0) return i;
        else return 1;
    }
}
