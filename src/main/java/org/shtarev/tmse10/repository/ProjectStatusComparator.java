package org.shtarev.tmse10.repository;

import org.shtarev.tmse10.entyty.Project;

import java.util.Arrays;
import java.util.Comparator;

public class ProjectStatusComparator implements Comparator<Project> {

    public int compare(Project a, Project b) {
       String aStatus = Arrays.toString(a.getTaskProjectStatus());
       String bStatus = Arrays.toString(b.getTaskProjectStatus());
        int i = aStatus.compareTo(bStatus);
        if (i != 0) return i;
        else return 1;
    }
}
