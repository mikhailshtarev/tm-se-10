package org.shtarev.tmse10.repository;

import org.shtarev.tmse10.entyty.Project;

import java.util.Comparator;

public class ProjectDataFinishComparator implements Comparator<Project> {

    public int compare(Project a, Project b) {
        assert a.getDataFinish() != null;
        assert b.getDataFinish() != null;
        int i = a.getDataFinish().compareTo(b.getDataFinish());
        if (i != 0) return i;
        else return 1;
    }
}
