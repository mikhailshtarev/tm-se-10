package org.shtarev.tmse10.repository;

import org.shtarev.tmse10.entyty.User;

import java.util.List;

public interface UserRepository<U> {
    void create(final User thisUser);

    List<U> getUserList();

    void rePassword(final String userID, final String newPassword);

    void userUpdate(final String name, final String thisUserID);
}
