package org.shtarev.tmse10.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse10.entyty.Task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaskRepositoryImpl implements TaskRepository<Task> {
    @Nullable
    private final Map<String, Task> taskMap = new HashMap<>();

    @Override
    public void persist(@NotNull final Task thisTask) {
        assert taskMap != null;
        if (!taskMap.containsValue(thisTask))
            taskMap.put(thisTask.getId(), thisTask);
    }

    @Override
    public void update(@NotNull final String taskId, @NotNull final Task thisTask) {
        assert taskMap != null;
        taskMap.forEach((s, task) -> {
            if (s.equals(taskId)) {
                task.setDescription(thisTask.getDescription());
                task.setDataStart(thisTask.getDataStart());
                task.setDataFinish(thisTask.getDataFinish());
                task.setTaskProjectStatus(thisTask.getTaskProjectStatus());
            }
        });

    }

    @Override
    public void remove(@NotNull final String id, @NotNull final String userId) {
        assert taskMap != null;
        @Nullable final Task thisTask = taskMap.get(id);
        assert thisTask != null;
        assert thisTask.getUserId() != null;
        if (thisTask.getUserId().equals(userId)) {
            taskMap.remove(id);
        }
    }


    @Override
    @Nullable
    public HashMap<String, Task> getTaskMap() {
        return (HashMap<String, Task>) taskMap;
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        assert taskMap != null;
        return taskMap.values().stream().filter(task -> task.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        assert taskMap != null;
        taskMap.values().stream().filter(task -> task.getUserId().equals(userId)).forEach(x -> taskMap.remove(x.getId()));
    }
}


