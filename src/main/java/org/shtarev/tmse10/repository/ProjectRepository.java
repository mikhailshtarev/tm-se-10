package org.shtarev.tmse10.repository;

import java.util.HashMap;

public interface ProjectRepository<T> {

    void persist(final T thisProject);

    void update(final String projectId, final T thisProject);

    void remove(final String Id, final String userID);

    HashMap<String, T> getProjectMap();

    void removeAll(final String userID);
}