package org.shtarev.tmse10.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse10.entyty.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectRepositoryImpl implements ProjectRepository<Project> {

    @NotNull
    private final HashMap<String, Project> projectMap = new HashMap<>();

    @Override
    public void persist(@NotNull final Project thisProject) {
        if (!projectMap.containsValue(thisProject))
            projectMap.put(thisProject.getId(), thisProject);
    }

    @Override
    public void update(@NotNull final String projectId,@NotNull final Project thisProject) {
        projectMap.forEach((s, project) ->{
            if (s.equals(projectId)) {
                project.setDescription(thisProject.getDescription());
                project.setDataStart(thisProject.getDataStart());
                project.setDataFinish(thisProject.getDataFinish());
                project.setTaskProjectStatus(thisProject.getTaskProjectStatus());
            }
        });

    }

    @Override
    public void remove(@NotNull final String Id, @NotNull final String userID) {
        @NotNull final Project thisProject = projectMap.get(Id);
        assert thisProject.getUserId() != null;
        if (thisProject.getUserId().equals(userID)) {
            projectMap.remove(Id);
        }
    }


    @Override
    public @NotNull HashMap<String, Project> getProjectMap() {
        return projectMap;
    }

    @Override
    public void removeAll(@NotNull final String userID) {
        @Nullable final List<Project> projectListValue = new ArrayList<>(projectMap.values());
        @Nullable final List<Project> project2List = projectListValue.stream().filter(project -> project.getUserId()
                .equals(userID)).collect(Collectors.toList());
        for (Project thisProject : project2List) {
            projectMap.remove(thisProject.getId());
        }
    }
}

