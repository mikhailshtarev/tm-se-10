package org.shtarev.tmse10.repository;

import java.util.HashMap;
import java.util.List;

public interface TaskRepository<T> {
    void persist(final T thisTask);

    void update (final String taskId,final T thisProject);

    void remove(final String id, final String userID);

    List<T> findAll(final String userID);

    HashMap<String, T> getTaskMap();

    void removeAll(final String userID);
}
