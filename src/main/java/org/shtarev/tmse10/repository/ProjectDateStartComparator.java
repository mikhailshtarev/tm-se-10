package org.shtarev.tmse10.repository;

import org.shtarev.tmse10.entyty.Project;

import java.util.Comparator;

public class ProjectDateStartComparator implements Comparator<Project> {

    public int compare(Project a, Project b) {
        assert a.getDataStart() != null;
        assert b.getDataStart() != null;
        int i = a.getDataStart().compareTo(b.getDataStart());
        if (i != 0) return i;
        else return 1;
    }
}
