package org.shtarev.tmse10.entyty;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse10.сommands.UserRole;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.UUID;

//@XmlRootElement(name = "user")

//@XmlType
@Getter
@Setter
public class User implements Serializable {
//    private static final long serialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String password;

    @Nullable
    private UserRole[] userRole = {UserRole.NO_ROLE};


    @NotNull
    private String userId = UUID.randomUUID().toString();

    public String toString() {
        String res = "Name: " + name + "   UserRole: " + userRole+ "   UserId: " + userId;
        return res;
    }
}
