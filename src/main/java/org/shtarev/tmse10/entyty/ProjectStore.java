package org.shtarev.tmse10.entyty;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

//@XmlRootElement(namespace = "org.shtarev.tmse10.entyty")

//@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@XmlRootElement(name = "projectStore")
public class ProjectStore {

    private String name;

    private List<Project> listProject;

}
