package org.shtarev.tmse10.entyty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

//@XmlRootElement(namespace = "org.shtarev.tmse10.entyty")
public class UserStore  {


//    public List<User> getUserList() {
//        return userList;
//    }
//
//    @XmlElement(name = "user")
     List<User> userList;
    private String name;

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
