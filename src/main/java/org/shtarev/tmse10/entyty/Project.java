package org.shtarev.tmse10.entyty;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse10.сommands.TaskProjectStatus;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;


@Getter
@Setter

//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder = {"name", "id", "description", "dataStart", "dataFinish", "userId", "taskProjectStatus", "dataCreate"}, name = "project")
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;


    @NotNull
    public final String id = UUID.randomUUID().toString();

    @Nullable
    private String name;


    @Nullable
    private String description;


    @Nullable
    private LocalDate dataStart;


    @Nullable
    private LocalDate dataFinish;


    @Nullable
    private String userId;


    @Nullable
    private TaskProjectStatus[] taskProjectStatus;


    @Nullable
    private LocalDate dataCreate;

//    @Override
//    public String toString() {
//        String ret = "id: " + id + "   name: " + name + "   description: " +
//                description + "   dataStart: " + dataStart + "  dataFinish: " +
//                dataFinish + "userID: " + userId + "   taskProjectStatus:   " + taskProjectStatus.toString() +
//                "   dateCreate" + dataCreate;
//        return ret;
//    }
//    @Override
//    public String toString() {
//        final StringBuilder sb = new StringBuilder("Project{");
//        sb.append("name = ").append(name).append('\'');
//        sb.append("id = ").append(id).append('\'');
//        sb.append("description = ").append(description).append('\'');
//        sb.append("DataStart = ").append(dataStart).append('\'');
//        sb.append("DataFinish = ").append(dataFinish).append('\'');
//        sb.append("userId = ").append(userId).append('\'');
//        sb.append("TPS = ").append(taskProjectStatus).append('\'');
//        sb.append("dataCreate = ").append(dataCreate).append('\'');
//        return  sb.toString();
//    }
}
