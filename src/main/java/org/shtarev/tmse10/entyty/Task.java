package org.shtarev.tmse10.entyty;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse10.сommands.TaskProjectStatus;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
public class Task {


    @NotNull
    private final String id = UUID.randomUUID().toString();

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "uuuu-MM-dd")
    private LocalDate dataStart;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "uuuu-MM-dd")
    private LocalDate dataFinish;

    @Nullable
    private String projectId;

    @Nullable
    private String UserId;

    @Nullable
    private TaskProjectStatus[] taskProjectStatus;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "uuuu-MM-dd")
    private LocalDate dataCreate;

}