package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse10.entyty.Project;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectList";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectService()
                .projectsReadAll(serviceLocator.getBootUser()
                        .getUserId());
        projectList.forEach(project -> {
                    System.out.println("Статус:" + Arrays.toString(project.getTaskProjectStatus()) +
                            " Название проекта: " + project.getName() + "  Описание проекта: "  + project.getDescription() + "   ID проекта: " + project.getId());
                    System.out.println("                ID User: " + project.getUserId() + "   Дата начала проекта: " + project.getDataStart() + "  " +
                            " Дата окончания проекта: " + project.getDataFinish());
                    System.out.println("                Дата создания в ТМ:  " + project.getDataCreate());
                    System.out.println();
                }
        );
    }
}
