package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

import static org.shtarev.tmse10.сommands.UserRole.REGULAR_USER;

final public class UserCreate extends AbstractCommand{
    @Override
    @NotNull
    public String getName() {
        return "UserCreate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create New User";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
    return UserRole.values();
    }

    @Override
    public void execute() {
        System.out.println("Введите имя нового пользователя: ");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите пароль для этого пользователя: ");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        @NotNull final UserRole[] userRole = {REGULAR_USER};
        serviceLocator.getUserService().create(name,password,userRole);
    }
}
