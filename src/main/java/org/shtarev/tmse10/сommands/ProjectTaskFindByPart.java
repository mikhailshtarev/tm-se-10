package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse10.entyty.Project;
import org.shtarev.tmse10.entyty.Task;

import java.util.ArrayList;
import java.util.Arrays;

public class ProjectTaskFindByPart extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectTaskFindPart";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Find Task and Project by part Name or Description";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите часть названия:  ");
        @NotNull final String findTP = serviceLocator.getTerminalService().nextLine();
        ArrayList<Project> sortProject = serviceLocator.getProjectService().findByDescription(findTP);
        ArrayList<Task> sortTask = serviceLocator.getTaskService().findByDescription(findTP);
        System.out.println("Список отсортированных проектов и задач, которые в описании и названии содержат :  " + findTP);
        sortProject.forEach(project -> {
                    System.out.println("Статус:" + Arrays.toString(project.getTaskProjectStatus()) +
                            " Название проекта: " + project.getName() + "  Описание проекта: " + project.getDescription() + "   ID проекта: " + project.getId());
                    System.out.println("                ID User: " + project.getUserId() + "   Дата начала проекта: " + project.getDataStart() + "  " +
                            " Дата окончания проекта: " + project.getDataFinish());
                    System.out.println("                Дата создания в ТМ:  " + project.getDataCreate());
                    System.out.println();
                }
        );
        sortTask.forEach(task -> {
                    System.out.println("Статус:" + Arrays.toString(task.getTaskProjectStatus()) +
                            " Название задачи: " + task.getName() + "  Описание задачи: " + task.getDescription() + "   ID проекта: " + task.getId());
                    System.out.println("                ID User: " + task.getUserId() + "   Дата начала проекта: " + task.getDataStart() + "  " +
                            " Дата окончания проекта: " + task.getDataFinish());
                    System.out.println("                Дата создания в ТМ:  " + task.getDataCreate());
                    System.out.println();
                }
        );
    }
}
