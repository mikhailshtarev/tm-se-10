package org.shtarev.tmse10.сommands;

import org.shtarev.tmse10.entyty.Project;
import org.shtarev.tmse10.entyty.ProjectStore;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class UserJaxbXMLWrite2 extends AbstractCommand {

    @Override
    public String getName() {
        return "UserJaxbXMLWrite2";
    }

    @Override
    public String getDescription() {
        return "Save Predmet oblast' with Externalizathion";
    }


    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    private static final String USERSTORE_XML = "src/main/resources/userList.xml";

    @Override
    public void execute() throws Exception, IOException, ClassNotFoundException {
        ProjectStore projectStore = new ProjectStore();
        projectStore.setName("Test 1");
        String userId = serviceLocator.getBootUser().getUserId();
        List<Project> prolist = serviceLocator.getProjectService().projectsReadAll(userId);
        projectStore.setListProject(prolist);
        JAXBContext context = JAXBContext.newInstance(ProjectStore.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(projectStore, System.out);
        marshaller.marshal(projectStore, new FileOutputStream(USERSTORE_XML));


    }
}
