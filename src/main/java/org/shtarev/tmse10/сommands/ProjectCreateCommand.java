package org.shtarev.tmse10.сommands;


import org.jetbrains.annotations.NotNull;

import static org.shtarev.tmse10.сommands.TaskProjectStatus.PLANNED;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectCreate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create a new Project";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите название проекта,который хотите создать: ");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите описание проекта: ");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала проекта в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dateStartSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания проекта в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dateFinishSP = serviceLocator.getTerminalService().nextLine();
        @NotNull final TaskProjectStatus[] taskProjectStatus = {PLANNED};
        serviceLocator.getProjectService().create(projectName, description, dateStartSP, dateFinishSP,serviceLocator
                .getBootUser().getUserId(),taskProjectStatus);

    }
}
