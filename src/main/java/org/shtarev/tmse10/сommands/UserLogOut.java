package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse10.entyty.User;

final public class UserLogOut extends AbstractCommand{
    @Override
    @NotNull
    public String getName() {
        return "UserLogout";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User ends the session";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        serviceLocator.setBootUser(new User());
    }
}
