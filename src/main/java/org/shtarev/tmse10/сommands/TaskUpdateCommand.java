package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

public class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "TaskUpdate";
    }

    @Override
    public String getDescription() {
        return "Update you Task";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите ID задачи,которую хотите изменить: ");
        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите новое описание задачи: ");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала задачи в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dateStartSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания задачи в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dateFinishSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите статус задачи(PLANNED , INTHEPROCESS , READY)");
        @NotNull final String taskProjectStatus = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getTaskService().update(taskId, description, dateStartSP, dateFinishSP,taskProjectStatus);

    }
}
