package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

final public class TaskClearCommand extends AbstractCommand{
    @Override
    @NotNull
    public String getName() {
        return "TaskClear";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all Tasks";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        serviceLocator.getTaskService().taskDeleteAll(serviceLocator.getBootUser().getUserId());
    }
}
