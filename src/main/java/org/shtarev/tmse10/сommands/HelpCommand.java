package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class HelpCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "help";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return UserRole.values();
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> lists = serviceLocator.getCommands();
        lists.forEach(abstractCommand -> System.out.println("Название команды: " + abstractCommand.getName() +
                "  Описание команды: " + abstractCommand.getDescription()));
    }
}
