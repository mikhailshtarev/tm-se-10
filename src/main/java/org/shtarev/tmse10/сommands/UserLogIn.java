package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

final public class UserLogIn extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserLogIn";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sign in";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return UserRole.values();
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Введите имя пользователя: ");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите пароль: ");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        serviceLocator.setBootUser(serviceLocator.getUserService().LogIn(name, password));
    }
}
