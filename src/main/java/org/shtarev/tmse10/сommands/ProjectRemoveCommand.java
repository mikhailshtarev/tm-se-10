package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

final public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectRemove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove selected project by its ID";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите ID проекта который хотите удалить:");
        @NotNull final String projectID = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getProjectService().delete(projectID,serviceLocator.getBootUser().getUserId());
    }
}
