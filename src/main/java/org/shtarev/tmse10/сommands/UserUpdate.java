package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse10.entyty.User;

final public class UserUpdate extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserUpdate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update user profile";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите новое имя  пользователя:");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        User thisUser = serviceLocator.getBootUser();
        @NotNull final String thisUserId = thisUser.getUserId();
        serviceLocator.getUserService().userUpdate(name, thisUserId);
    }
}
