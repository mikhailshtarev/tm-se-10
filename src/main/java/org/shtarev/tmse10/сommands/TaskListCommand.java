package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse10.entyty.Task;

import java.util.Arrays;
import java.util.List;

final public class TaskListCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "TaskList";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all Tasks";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        @NotNull final List<Task> taskList = serviceLocator.getTaskService().taskReadAll(serviceLocator.getBootUser().getUserId());
        taskList.forEach(task -> {
            System.out.println("Статус:" + Arrays.toString(task.getTaskProjectStatus()) +
                    "Название задачи: " + task.getName()+ "  Описание задачи: "  + task.getDescription());
            System.out.println("                ID задачи:  " + task.getId() +  "  ID проекта к которому относится задача:  " + task.getProjectId());
            System.out.println("                Дата начала проекта: " + task.getDataStart() + "    Дата окончания проекта: " +task.getDataFinish());
            System.out.println("                Дата создания в ТМ: " + task.getDataCreate());
            System.out.println();
        });

    }
}