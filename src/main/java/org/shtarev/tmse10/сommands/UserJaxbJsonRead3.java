package org.shtarev.tmse10.сommands;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.shtarev.tmse10.entyty.Task;

import java.io.File;
import java.util.List;

public class UserJaxbJsonRead3 extends AbstractCommand {
    @Override
    public String getName() {
        return "UserJsonRead3";
    }

    @Override
    public String getDescription() {
        return "Read Predmet oblast' with serializable JSON";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
        List<Task> task= mapper.readValue(new File("c:\\projects\\tm-se-10\\user.json"), new TypeReference<List<Task>>() {
        });
        task.forEach(task1 ->System.out.println(task1.getId()+ task1.getName()));
    }
}