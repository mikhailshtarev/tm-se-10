package org.shtarev.tmse10.сommands;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.shtarev.tmse10.entyty.Task;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class UserJaxbJsonWrite3 extends AbstractCommand {
    @Override
    public String getName() {
        return "UserJsonWrite3";
    }

    @Override
    public String getDescription() {
        return "Write Predmet oblast' with serializable JSON";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    private final static String baseFile = "user.json";

    @SneakyThrows
    @Override
    public void execute() throws Exception, IOException {
        String userId = serviceLocator.getBootUser().getUserId();
        List<Task> taskList = serviceLocator.getTaskService().taskReadAll(userId);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.registerModule(new JavaTimeModule());
        mapper.writeValue(new File("c:\\projects\\tm-se-10\\user.json"), taskList);
    }
}
