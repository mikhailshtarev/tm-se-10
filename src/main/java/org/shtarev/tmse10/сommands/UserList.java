package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse10.entyty.User;

import java.util.List;

final public class UserList extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "UserList";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "View all Users";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        @NotNull final List<User> userNameList = serviceLocator.getUserService().getUserList();
        userNameList.forEach(user -> System.out.println("Имя пользователя: " + user.getName() + "  ID пользователя: " + user.getUserId()));
    }
}
