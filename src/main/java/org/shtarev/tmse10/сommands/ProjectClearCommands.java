package org.shtarev.tmse10.сommands;


import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommands extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectClear";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete all Project with her tasks";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        serviceLocator.getProjectService().projectDeleteAll(serviceLocator.getBootUser().getUserId());
    }
}
