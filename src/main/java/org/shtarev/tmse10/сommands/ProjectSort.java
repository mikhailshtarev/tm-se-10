package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;
import org.shtarev.tmse10.entyty.Project;

import java.util.Arrays;
import java.util.Set;

public class ProjectSort extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "ProjectSort";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sort project by date";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("По какому признаку отсортировать список: ");
        System.out.println(" 1 - по дате начала проекта ");
        System.out.println(" 2 - по дате окончания проекта ");
        System.out.println(" 3 - по дате создания проекта ");
        System.out.println(" 4 - по статусу готовности проекта ");
        @NotNull final String sortedNumber = serviceLocator.getTerminalService().nextLine();
        Set<Project> sortProject = serviceLocator.getProjectService().sorted(sortedNumber);
        System.out.println("Список отсортированных проектов:");
        sortProject.forEach(project -> {
                    System.out.println("Статус:" + Arrays.toString(project.getTaskProjectStatus()) +
                            " Название проекта: " + project.getName() + "  Описание проекта: " + project.getDescription() + "   ID проекта: " + project.getId());
                    System.out.println("                ID User: " + project.getUserId() + "   Дата начала проекта: " + project.getDataStart() + "  " +
                            " Дата окончания проекта: " + project.getDataFinish());
                    System.out.println("                Дата создания в ТМ:  " + project.getDataCreate());
                    System.out.println();
                }
        );
    }
}
