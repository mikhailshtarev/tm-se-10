package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

import static org.shtarev.tmse10.сommands.TaskProjectStatus.PLANNED;

final public class TaskCreateCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "TaskCreate";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new Task";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите название задачи:");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите описание задачи:");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала задачи в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dataStart = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания задачи в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dataFinish = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите ID проекта, к которому относится задача: ");
        @NotNull final String projectID = serviceLocator.getTerminalService().nextLine();
        @NotNull final String thisUserID = serviceLocator.getBootUser().getUserId();
        @NotNull final TaskProjectStatus[] taskProjectStatus = {PLANNED};
        serviceLocator.getTaskService().create(name, description, dataStart, dataFinish, projectID, thisUserID, taskProjectStatus);

    }
}
