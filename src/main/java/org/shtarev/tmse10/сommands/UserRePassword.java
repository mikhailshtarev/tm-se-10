package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

final public class UserRePassword extends AbstractCommand{
    @Override
    @NotNull
    public String getName() {
        return "UserRePassword";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update you password";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите имя пользователя, у которого хотите поменять пароль: ");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите старый пароль: ");
        @NotNull final  String oldPassword = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите новый пароль: ");
        @NotNull final String newPassword = serviceLocator.getTerminalService().nextLine();
        if (!serviceLocator.getBootUser().getName().equals(name))
        {System.out.println("wrong username or password");
        return;}
        serviceLocator.getUserService().rePassword(name, oldPassword,newPassword);
    }
}
