package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

final public class TaskRemoveCommand extends AbstractCommand{
    @Override
    @NotNull
    public String getName() {
        return "TaskRemove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove selected Task";
    }

    @Override
    @NotNull
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN,UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите ID задачи которую хотите удалить:");
        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getTaskService().delete(taskId,serviceLocator.getBootUser().getUserId());
    }
}
