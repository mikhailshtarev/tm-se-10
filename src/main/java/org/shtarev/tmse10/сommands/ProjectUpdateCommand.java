package org.shtarev.tmse10.сommands;

import org.jetbrains.annotations.NotNull;

public class ProjectUpdateCommand extends AbstractCommand{
    @Override
    public String getName()  {
        return "ProjectUpdate";
    }

    @Override
    public String getDescription()  {
        return "Update you Project";
    }

    @Override
    public UserRole[] getListRole() {
        return new UserRole[]{UserRole.ADMIN, UserRole.REGULAR_USER};
    }

    @Override
    public void execute() {
        System.out.println("Введите ID проекта,который хотите изменить: ");
        @NotNull final String projectId = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите новое описание проекта: ");
        @NotNull final String description = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату начала проекта в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dateStartSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите дату окончания проекта в формате uuuu-MM-dd HH:mm:ss: ");
        @NotNull final String dateFinishSP = serviceLocator.getTerminalService().nextLine();
        System.out.println("Введите статус проета(PLANNED , INTHEPROCESS , READY)");
        @NotNull final String taskProjectStatus = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getProjectService().update(projectId, description, dateStartSP, dateFinishSP,taskProjectStatus);
    }
}
