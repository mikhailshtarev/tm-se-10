package org.shtarev.tmse10.service;

import org.shtarev.tmse10.сommands.TaskProjectStatus;

import java.util.ArrayList;
import java.util.List;

public interface TaskService<T> {

    void create(final String name, final String description, final String dataStart,
                final String dataFinish, final String projectID, final String thisUserID,
                final TaskProjectStatus[] taskProjectStatus);
    void update (final String taskId,final String  description,final String  dateStartSP,final String  dateFinishSP,
                 final String taskProjectStatus);

    ArrayList<T> findByDescription (final String partName);

    void delete(final String id, final String userId);

    List<T> taskReadAll(final String userId);

    void taskDeleteAll(final String userId);
}
