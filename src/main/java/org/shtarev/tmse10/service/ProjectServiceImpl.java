package org.shtarev.tmse10.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse10.entyty.Project;
import org.shtarev.tmse10.entyty.Task;
import org.shtarev.tmse10.repository.*;
import org.shtarev.tmse10.сommands.TaskProjectStatus;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static org.shtarev.tmse10.сommands.TaskProjectStatus.*;


public class ProjectServiceImpl implements ProjectService<Project> {
    @NotNull
    private final ProjectRepository<Project> projectRepository;
    @NotNull
    private final TaskRepository<Task> taskRepository;
    @NotNull
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
            .ofPattern("uuuu-MM-dd HH:mm:ss")
            .withResolverStyle(ResolverStyle.STRICT);

    @NotNull
    public ProjectServiceImpl(@NotNull ProjectRepository<Project> projectRepository, @NotNull TaskRepository<Task> taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@NotNull final String name, @NotNull final String description, @NotNull final String startDate,
                       @NotNull final String finishDate, @NotNull final String thisUserId,
                       @NotNull final TaskProjectStatus[] taskProjectStatus) {
        if (name.isEmpty() || description.isEmpty() || startDate.isEmpty() || finishDate.isEmpty()) {
            return;
        }
        @NotNull final LocalDate startDateTR = LocalDate.parse(startDate, dateTimeFormatter);
        @NotNull final LocalDate finishDateTR = LocalDate.parse(finishDate, dateTimeFormatter);
        @NotNull final LocalDate createDateTR = LocalDate.now();
        @NotNull final Project thisProject = new Project();
        thisProject.setName(name);
        thisProject.setDescription(description);
        thisProject.setDataStart(startDateTR);
        thisProject.setDataFinish(finishDateTR);
        thisProject.setUserId(thisUserId);
        thisProject.setTaskProjectStatus(taskProjectStatus);
        thisProject.setDataCreate(createDateTR);
        projectRepository.persist(thisProject);
    }

    @Override
    public void update(@NotNull final String projectId, @NotNull final String description,
                       @NotNull final String dateStartSP, @NotNull final String dateFinishSP,
                       @NotNull final String taskProjectStatus) {
        if (projectId.isEmpty() || description.isEmpty() || dateStartSP.isEmpty() || dateFinishSP.isEmpty()
                || taskProjectStatus.isEmpty())
            return;
        @NotNull final LocalDate startDateTR = LocalDate.parse(dateStartSP, dateTimeFormatter);
        @NotNull final LocalDate finishDateTR = LocalDate.parse(dateFinishSP, dateTimeFormatter);
        @NotNull final Project thisProject = new Project();
        @NotNull TaskProjectStatus[] taskProjectStatus1 = new TaskProjectStatus[0];
        if (taskProjectStatus.equals("INTHEPROCESS")) {
            taskProjectStatus1 = new TaskProjectStatus[]{IN_THE_PROCESS};
        }
        if (taskProjectStatus.equals("READY")) {
            taskProjectStatus1 = new TaskProjectStatus[]{READY};
        }
        if (taskProjectStatus.equals("PLANNED")) {
            taskProjectStatus1 = new TaskProjectStatus[]{PLANNED};
        }
        thisProject.setDescription(description);
        thisProject.setDataStart(startDateTR);
        thisProject.setDataFinish(finishDateTR);
        thisProject.setTaskProjectStatus(taskProjectStatus1);
        projectRepository.update(projectId, thisProject);
    }

    @Override
    public TreeSet<Project> sorted(@NotNull final String sort) {
        switch (sort) {
            case "1":
                TreeSet<Project> projects = new TreeSet<>(new ProjectDateStartComparator());
                projects.addAll(projectRepository.getProjectMap().values());
                return projects;
            case "2":
                TreeSet<Project> projects1 = new TreeSet<>(new ProjectDataFinishComparator());
                projects1.addAll(projectRepository.getProjectMap().values());
                return projects1;
            case "3":
                TreeSet<Project> projects2 = new TreeSet<>(new ProjecDataCreateComparetor());
                projects2.addAll(projectRepository.getProjectMap().values());
                return projects2;
            case "4":
                TreeSet<Project> projects3 = new TreeSet<>(new ProjectStatusComparator());
                projects3.addAll(projectRepository.getProjectMap().values());
                return projects3;
            default:
                return null;
        }
    }

    @Override
    public ArrayList<Project> findByDescription(@NotNull final  String partName) {
        @NotNull final Collection<Project> projectList = projectRepository.getProjectMap().values();
        ArrayList<Project> projectList2 = new ArrayList<>();
        projectList.forEach(project -> {
            if ((project.getName().contains(partName)) || (project.getDescription().contains(partName)))
                projectList2.add(project);
        });
        return projectList2;
    }


    @Override
    public void delete(@NotNull final String id, @NotNull final String userID) {
            if (id.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;            }
            projectRepository.remove(id, userID);
            taskRepository.remove(id, userID);
            }

    @Override
    @Nullable
    public List<Project> projectsReadAll(@NotNull final String userID) {
        @Nullable final List<Project> projectListValue = new ArrayList<>(projectRepository.getProjectMap().values());
        return projectListValue.stream().filter(project -> project.getUserId().equals(userID)).collect(Collectors.toList());

    }

    @Override
    public void projectDeleteAll(@NotNull final String userId) {
        projectRepository.removeAll(userId);
        taskRepository.removeAll(userId);
    }
}

