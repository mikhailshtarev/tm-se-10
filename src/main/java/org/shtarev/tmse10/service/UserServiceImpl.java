package org.shtarev.tmse10.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse10.entyty.User;
import org.shtarev.tmse10.repository.UserRepository;
import org.shtarev.tmse10.сommands.UserRole;

import java.util.List;

public class UserServiceImpl implements UserService<User> {

    @NotNull
    private final UserRepository<User> userRepository;


    @NotNull
    public UserServiceImpl(@NotNull UserRepository<User> userRepository) {
        this.userRepository = userRepository;
    }

    public void create(@NotNull final String name, @NotNull final String password, @NotNull final UserRole[] userRole) {
        try {
            if (name.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            @NotNull final User thisUser = new User();
            thisUser.setName(name);
            thisUser.setPassword(DigestUtils.md5Hex(password));
            thisUser.setUserRole(userRole);
            userRepository.create(thisUser);
        } catch (
                NullPointerException e) {
            System.out.println(e.getMessage() + "Error");
        }
    }

    @Nullable
    public List<User> getUserList() {
        return userRepository.getUserList();
    }

    @Override
    @Nullable
    public User LogIn(@NotNull final String name, @NotNull final String password) {
        @NotNull final User newUser = new User();
        try {
            if (name.isEmpty() || password.isEmpty()) {
                System.out.println("One of the fields is empty, please try again");
                return newUser;
            }
            @NotNull final List<User> userListUL = userRepository.getUserList();
            @NotNull final String thisPassword = DigestUtils.md5Hex(password);
            for (User user : userListUL) {
                if (user.getName().equals(name) && user.getPassword().equals(thisPassword)) {
                    return user;
                }
            }
        } catch (NullPointerException e) {
            System.out.println(e.getMessage() + "  !  Incorrect console input, please try again. Error");
        }
        System.out.println("Incorrect name or password, please try again.");
        return newUser;
    }

    @Override
    public void rePassword(@NotNull String name, @NotNull String oldPassword, @NotNull String newPassword) {

        if (name.isEmpty() || newPassword.isEmpty()) {
            System.out.println("One of the fields is empty, please try again");
            return;
        }
        @NotNull final List<User> userCollect = userRepository.getUserList();
        for (User thisUser : userCollect) {
            String oldPassword2 = DigestUtils.md5Hex(oldPassword);
            if (thisUser.getName().equals(name) || thisUser.getPassword().equals(oldPassword2)) {
                userRepository.rePassword(thisUser.getUserId(), newPassword);
            } else System.out.println("Username or password does not match");
        }
    }

    @Override
    public void userUpdate(@NotNull String name, @NotNull String thisUserId) {

        if (name.isEmpty() || thisUserId.isEmpty()) {
            System.out.println("Одно из полей пустое,попробуйте ввести заново");
            return;
        }
        userRepository.userUpdate(name, thisUserId);
    }
}

