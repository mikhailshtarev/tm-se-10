package org.shtarev.tmse10.service;

import org.shtarev.tmse10.сommands.TaskProjectStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public interface ProjectService<T> {

    void create(final String name, final String description, final String startDate, final String finishDate,
                final String thisUserId, final TaskProjectStatus[] taskProjectStatus);

    void update(final String projectId, final String description, final String dateStartSP, final String dateFinishSP,
                final String taskProjectStatus);

    Set<T> sorted(final String sort);

    ArrayList<T> findByDescription(final String partName);

    void delete(final String id, final String userID);

    List<T> projectsReadAll(final String userID);

    void projectDeleteAll(final String userID);
}
