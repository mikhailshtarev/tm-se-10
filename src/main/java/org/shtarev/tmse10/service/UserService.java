package org.shtarev.tmse10.service;

import org.shtarev.tmse10.entyty.User;
import org.shtarev.tmse10.сommands.UserRole;

import java.util.List;

public interface UserService<U> {
    void create(final String name, final String password, final UserRole[] userRole);

    List<U> getUserList();

    User LogIn(final String name, final String password) throws Exception;

    void rePassword(final String name,final String oldPassword,final String newPassword);

    void userUpdate(final String name,final String thisUserId);
}