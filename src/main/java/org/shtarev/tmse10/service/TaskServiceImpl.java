package org.shtarev.tmse10.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse10.entyty.Project;
import org.shtarev.tmse10.entyty.Task;
import org.shtarev.tmse10.repository.ProjectRepository;
import org.shtarev.tmse10.repository.TaskRepository;
import org.shtarev.tmse10.сommands.TaskProjectStatus;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.shtarev.tmse10.сommands.TaskProjectStatus.*;

public class TaskServiceImpl implements TaskService<Task> {
    @NotNull
    private final TaskRepository<Task> taskRepository;

    @NotNull
    private final ProjectRepository<Project> projectRepository;
    @NotNull
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
            .ofPattern("uuuu-MM-dd HH:mm:ss")
            .withResolverStyle(ResolverStyle.STRICT);

    @NotNull
    public TaskServiceImpl(final @NotNull TaskRepository<Task> taskRepository, final @NotNull ProjectRepository<Project> projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(@NotNull final String name, @NotNull final String description, @NotNull final String dataStart,
                       @NotNull final String dataFinish, @NotNull final String projectID,
                       @NotNull final String thisUserID, @NotNull TaskProjectStatus[] taskProjectStatus) {
            if (name.isEmpty() || description.isEmpty() || dataStart.isEmpty() || dataFinish.isEmpty()) {
                System.out.println("Одно из полей пустое,попробуйте ввести заново");
                return;
            }
            projectRepository.getProjectMap().keySet().forEach(s ->
            {
                if (s.equals(projectID)) {
                    @NotNull final LocalDate dataStartTR = LocalDate.parse(dataStart, dateTimeFormatter);
                    @NotNull final LocalDate dataFinishTR = LocalDate.parse(dataFinish, dateTimeFormatter);
                    @NotNull final LocalDate createDateTR = LocalDate.now();
                    @NotNull final Task thisTask = new Task();
                    thisTask.setName(name);
                    thisTask.setDescription(description);
                    thisTask.setDataStart(dataStartTR);
                    thisTask.setDataFinish(dataFinishTR);
                    thisTask.setProjectId(projectID);
                    thisTask.setUserId(thisUserID);
                    thisTask.setDataCreate(createDateTR);
                    thisTask.setTaskProjectStatus(taskProjectStatus);
                    taskRepository.persist(thisTask);
                }
            });
    }

    @Override
    public void update(@NotNull final String taskId, @NotNull final String description,
                       @NotNull final String dateStartSP, @NotNull final String dateFinishSP,
                       @NotNull final String taskProjectStatus) {
        if (taskId.isEmpty() || description.isEmpty() || dateStartSP.isEmpty() || dateFinishSP.isEmpty()
                || taskProjectStatus.isEmpty()) {
            System.out.println("Одно из полей пустое,попробуйте ввести заново");
            return;
        }
        @NotNull final LocalDate startDateTR = LocalDate.parse(dateStartSP, dateTimeFormatter);
        @NotNull final LocalDate finishDateTR = LocalDate.parse(dateFinishSP, dateTimeFormatter);
        @NotNull final Task thisTask = new Task();
        @NotNull TaskProjectStatus[] taskProjectStatus1 = new TaskProjectStatus[0];
        if (taskProjectStatus.equals("INTHEPROCESS")) {
            taskProjectStatus1 = new TaskProjectStatus[]{IN_THE_PROCESS};
        }
        if (taskProjectStatus.equals("READY")) {
            taskProjectStatus1 = new TaskProjectStatus[]{READY};
        }
        if (taskProjectStatus.equals("PLANNED")) {
            taskProjectStatus1 = new TaskProjectStatus[]{PLANNED};
        }
        thisTask.setDescription(description);
        thisTask.setDataStart(startDateTR);
        thisTask.setDataFinish(finishDateTR);
        thisTask.setTaskProjectStatus(taskProjectStatus1);
        taskRepository.update(taskId, thisTask);
    }

    public ArrayList<Task> findByDescription(@NotNull final String partName) {
        @NotNull final Collection<Task> taskList = taskRepository.getTaskMap().values();
        ArrayList<Task> taskList2 = new ArrayList<>();
        taskList.forEach(task -> {
            if ((task.getName().contains(partName)) || (task.getDescription().contains(partName)))
                taskList2.add(task);
        });
        return taskList2;
    }


    @Override
    public void delete(@NotNull final String id, @NotNull final String userId) {
        if (id.isEmpty()) {
            System.out.println("Одно из полей пустое,попробуйте ввести заново");
            return;
        }
        taskRepository.remove(id, userId);
    }

    @Override
    @Nullable
    public List<Task> taskReadAll(@NotNull final String userId) {
        return taskRepository.findAll(userId);
    }

    @Override
    public void taskDeleteAll(@NotNull final String userId) {
        taskRepository.removeAll(userId);
    }
}





