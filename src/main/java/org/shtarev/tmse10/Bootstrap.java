package org.shtarev.tmse10;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.shtarev.tmse10.entyty.Project;
import org.shtarev.tmse10.entyty.Task;
import org.shtarev.tmse10.entyty.User;
import org.shtarev.tmse10.repository.*;
import org.shtarev.tmse10.service.*;
import org.shtarev.tmse10.сommands.AbstractCommand;
import org.shtarev.tmse10.сommands.ServiceLocator;
import org.shtarev.tmse10.сommands.UserRole;

import java.util.*;
import java.util.stream.Collectors;

import static org.shtarev.tmse10.сommands.UserRole.*;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final TaskRepository<Task> taskRepository = new TaskRepositoryImpl();
    @NotNull
    private final ProjectRepository<Project> projectRepository = new ProjectRepositoryImpl();
    @NotNull
    private final UserRepository<User> userRepository = new UserRepositoryImpl();
    @NotNull
    private final UserService<User> userService = new UserServiceImpl(userRepository);
    @NotNull
    private final TaskService<Task> taskService = new TaskServiceImpl(taskRepository, projectRepository);
    @NotNull
    private final ProjectService<Project> projectService = new ProjectServiceImpl(projectRepository, taskRepository);
    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    public User bootUser = new User();
    @NotNull
    public final Scanner terminalService = new Scanner(System.in);

    @Override
    @NotNull
    public ProjectService<Project> getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public TaskService<Task> getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public UserService<User> getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public User getBootUser() {
        return bootUser;
    }

    @Override
    public void setBootUser(@NotNull User thisUser) {
        bootUser = thisUser;
    }

    @Override
    @NotNull
    public Scanner getTerminalService() {
        return terminalService;
    }

    final protected void createTwoUser() {
        @NotNull final UserRole[] userRole = {ADMIN};
        @NotNull final UserRole[] userRole2 = {REGULAR_USER};
        userService.create("admin", "admin", userRole);
        userService.create("regularUser", "regularUser", userRole2);

    }

protected void init(Set<Class<?>> classes) {
        try {
            for (@NotNull final Class cl : classes) {
                Object o = cl.newInstance();
                if (o instanceof AbstractCommand) {
                    registry((AbstractCommand) o);
                } else throw new IllegalArgumentException("error");
            }
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }


    protected void registry(@NotNull final AbstractCommand command) {
        @NotNull final String cliCommand = command.getName();
        @NotNull final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (cliDescription == null || cliDescription.isEmpty())
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    protected void start() {
        try {
            System.out.println("*** WELCOME TO TASK MANAGER ***");
            String command = "";
            while (!"exit".equals(command)) {
                        List<UserRole> listRole = Arrays.stream(bootUser.getUserRole()).collect(Collectors.toList());
                        System.out.print("Role: " + listRole.stream().findAny() +
                                " |  Enter the terminal command: ");

                    command = terminalService.nextLine();
                    execute(command);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void execute(final String command) throws Exception {
        try {
            @NotNull final AbstractCommand abstractCommand = commands.get(command);
            @NotNull UserRole[] userRoleList = abstractCommand.getListRole();
            @Nullable UserRole[] userRoleList2 = bootUser.getUserRole();
            for (UserRole thisRole : userRoleList) {
                for (UserRole thisRole2 : userRoleList2) {
                    if (thisRole.equals(thisRole2)) {
                        abstractCommand.execute();
                        return;
                    }
                }
            }
        } catch (NullPointerException e) {
            System.out.print(e.getMessage());
            System.out.println(" command entered incorrectly");
        }
    }
}


